package com.todo.todo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TodoController {

    @GetMapping("/Task")
    public String getTask() {
        return "Displaying the list of tasks";
    }

    @GetMapping("/Task/taskId/{id}")
    public String getId(@PathVariable int id) {
        return "Task id is " + id;
    }

    @GetMapping("/Task/taskTitle/{title}")
    public String getTitle(@PathVariable String title) {
        return "Task title is " + title;
    }

    @GetMapping("/Task/taskDesc/{desc}")
    public String getDescription(@PathVariable String desc) {
        return "Task description is " + desc;
    }

    @GetMapping("/Task/taskStatus/{status}")
    public String getStatus(@PathVariable boolean status) {
        String word;
        if (status)
            word = "Completed";
        else
            word = "Pending";
        return "Task status is " + word;
    }

}
